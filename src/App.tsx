import './App.scss';

import Button from '@mui/material/Button';
import Grid from '@mui/material/Grid';
import Typography from '@mui/material/Typography';
import React, {useCallback, useState} from 'react';

const App: React.FC = () => {
  const [count, setCount] = useState(0);

  const increment = useCallback(() => {
    setCount(count => count + 1);
  }, []);

  const decrement = useCallback(() => {
    setCount(count => count - 1);
  }, []);

  return (
    <Grid
      container
      direction="column"
      spacing={12}
      alignItems="center"
      justifyContent="center"
    >
      <Grid item xs={1}>
        <Grid container spacing={0} alignItems="center" justifyContent="center">
          <Grid item xs={0}>
            <Typography align="center" variant="h2">
              {count}
            </Typography>
          </Grid>
        </Grid>
      </Grid>
      <Grid container spacing={0} alignItems="center" justifyContent="center">
        <Grid item xs={3}>
          <Button fullWidth size="medium" onClick={() => decrement()}>
            <Typography variant="h2">-</Typography>
          </Button>
        </Grid>
        <Grid item xs={3}>
          <Button fullWidth size="medium" onClick={() => increment()}>
            <Typography variant="h2">+</Typography>
          </Button>
        </Grid>
      </Grid>
    </Grid>
  );
};

export default App;
